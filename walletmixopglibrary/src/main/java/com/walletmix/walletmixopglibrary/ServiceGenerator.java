package com.walletmix.walletmixopglibrary;

class ServiceGenerator {
    static  <T> T createService(Class<T> service) {
        return ApiClient.getRetrofitApiClient().create(service);
    }
}
