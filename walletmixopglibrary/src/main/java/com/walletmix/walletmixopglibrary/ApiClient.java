package com.walletmix.walletmixopglibrary;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class ApiClient {

    private static Retrofit retrofit = null;

    static Retrofit getRetrofitApiClient() {

        if (retrofit == null) {
            //HttpClient Builder....
            OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
            okHttpClientBuilder.connectTimeout(20, TimeUnit.SECONDS);
            okHttpClientBuilder.readTimeout(20, TimeUnit.SECONDS);
            okHttpClientBuilder.writeTimeout(20, TimeUnit.SECONDS);

            // Building Retrofit...
            retrofit = new Retrofit.Builder()
                    .baseUrl("https://walletmix.info") // Dummy Base url..because we are using dynamic url...
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClientBuilder.build())
                    .build();
        }
        return retrofit;
    }
}
