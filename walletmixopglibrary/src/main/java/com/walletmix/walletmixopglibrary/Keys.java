package com.walletmix.walletmixopglibrary;
enum  Keys {
    data_bundle, is_live, token, card_selection_url, call_back_url, wmx_id, access_api_key, authorization, call_back_activity_class_name;
}
